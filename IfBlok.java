public class IfBlok {

    public  static void raschet(int day) {
        if (day < 1) {
          //  System.out.println("Если поход менее, чем на сутки, то взять только воды и еды");
            day1();
        }
        if (day >= 1 && day < 3) {
          //  System.out.println("Если поход от суток до 3х, то взять воду, еду, палатку");
            day2();
        }
        if (day >= 3) {
          //  System.out.println("Если поход от 3х суток и более — воду, еду, палатку, запасные вещи");
            day3();
        }
    }
    public static void day1() {
        System.out.println("Если поход менее, чем на сутки, то взять только воды и еды");
    }
    public static void day2() {
        System.out.println("Если поход от суток до 3х, то взять воду, еду, палатку");
    }
    public static void day3() {
        System.out.println("Если поход от 3х суток и более — воду, еду, палатку, запасные вещи");
    }

    public static void main(String[] args) {
        raschet(3);
    }
}
