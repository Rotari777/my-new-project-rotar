public class Lesson03 {

    byte aByte;
    int anInt;
    short aShort;
    long aLong;
    float aFloat;
    double aDouble;
    char aChar;
    boolean aBoolean;

    {
        System.out.println("Love Java");
    }

    public Lesson03() {
        aByte = 1;
        anInt = 3;
        aShort = 3;
        aLong = 3;
        aFloat = 3.4F;
        aDouble = 4.5;
        aChar = 'L';
        aBoolean = true;
    }


    {
        System.out.println("aByte");
        System.out.println("anInt");
        System.out.println("aShort");
        System.out.println("aLong");
        System.out.println("aFloat");
        System.out.println("aDouble");
        System.out.println("aChar");
        System.out.println("aBoolean");
    }
    public static void main(String[] args) { }
}
